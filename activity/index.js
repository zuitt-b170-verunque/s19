// console.log("Hello")

/* 

Activity:
// 1. In the S19 folder, create an activity folder and an index.html and index.js file inside of it.
// 2. Link the script.js file to the index.html file.

// 3. Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)

// 4. Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…

// 5. Create a variable address with a value of an array containing details of an address.

// 6. Destructure the array and print out a message with the full address using Template Literals.

// 7. Create a variable animal with a value of an object data type with different animal details as it’s properties.

// 8. Destructure the object and print out a message with the details of the animal using Template Literals.

// 9. Create an array of numbers.

// 10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.

// 11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.

// 12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.

// 13. Create/instantiate a new object from the class Dog and console log the object.

14. Create a git repository named S19.

15. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
16. Add the link in Boodle.

*/

// exponent
const getCube = 5**3;
console.log(`The cube of 5 is ${getCube}`);

let address = ["Brgy. Fatima"," Villaba", "Leyte"]

// array deconstruction
const [brgy, municipality, province] = address
console.log(`I live at ${address}`)


// object
let animal = {
    Name: "Lolong",
    kind: "saltwater crocodile",
    weight: "1075 kg",
    measurement: "20 ft. 3 inch"
}

// object deconstruction
const{Name, kind, weight, measurement} = animal;
console.log(`${Name} was a ${kind}. He weighed ${weight} with a measurement of ${measurement}`)


let numbers = [1, 2, 3, 4, 5];



numbers.forEach(pick => console.log(pick));


// let reduceNumber = numbers.reduce (
    //     function (a,b)
    //     {
        //         return a + b;
        //     }
        // )
        // console.log(reduceNumber);
        
        let reduceNumber = numbers.reduce(
            (a,b) => a + b
            ) 
            console.log(reduceNumber);



// 12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.

class Dog 
{
    constructor(name, age, breed)
    {
        this.name = name,
        this.age = age,
        this.breed = breed
    }
}

let Dog2 = new Dog ("Chamcham", 2, "shi tzu");
console.log(Dog2)